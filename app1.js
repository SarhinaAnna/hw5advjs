const promise1 = fetch("https://ajax.test-danit.com/api/json/users")
  .then((response) => {
    return response.json();
  })
  .then((users) => {
    console.log(users);
    return users;
  });

const promise2 = fetch("https://ajax.test-danit.com/api/json/posts")
  .then((response) => {
    return response.json();
  })
  .then((posts) => {
    console.log(posts);
    return posts;
  });

class Card {
  constructor(title, content, name, lastname, email, postId) {
    this.title = title;
    this.content = content;
    this.name = name;
    this.lastname = lastname;
    this.email = email;
    this.postId = postId;
    this.createCard();
  }
  createCard() {
    document.body.insertAdjacentHTML(
      "afterbegin",
      `<div class="cards">
     
                  <div class="name"><b> ${this.name} ${this.lastname}</b></div>
                  <div class="email">${this.email}</div>
                  <div class="title"><b>${this.title}</b></div>
                  <div class="content">${this.content}</div>
                  <button class="btn-delete" data-post="${this.postId}">Delete</button>
                  </div>
                  `
    );
  }
}

Promise.all([promise1, promise2]).then((values) => {
  console.log(values);
  const [users, posts] = values;
  posts.forEach((post) => {
    const user = users.find((elem) => {
      console.log(post.userId);
      if (post.userId === elem.id) {
        return true;
      }
    });

    const card = new Card(
      post.title,
      post.body,
      user.name,
      user.username,
      user.email,
      post.id
    );
  });
});

document.body.addEventListener("click", function (event) {
  if (event.target.className === `btn-delete`) {
    const postId = event.target.dataset.post;
    fetch(`https://ajax.test-danit.com/api/json/posts/${postId}`, {
      method: "DELETE",
    }).then((response) => event.target.closest(".cards").remove());
  }
});
